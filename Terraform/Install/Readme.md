# Terraform 설치

 ###### wget 설치
 ```bash
 yum install wget
 ```
 
 ###### Terraform 최신 버전 설치(홈페이지 접속 후 직접 다운로드)
 ###### https://www.terraform.io/downloads.html


 ###### Terraform 특정 버전 설치
 ```bash
 wget https://releases.hashicorp.com/terraform/0.15.1/terraform_0.15.1_linux_amd64.zip

 unzip terraform_0.12.29_linux_amd64.zip

 ```

 ###### 환경변수 위치로 이동
 ```bash
 sudo mv terraform /usr/local/bin
 ```

 ###### 버전 확인
 ```bash
 terraform --version
 ```

 ###### 환경변수 추가
 ```bash
 export PATH=$PATH:/home/ec2-user/
 export PATH=$PATH:/home/ec2-user/terraform-example/terraform-export/
 ````
