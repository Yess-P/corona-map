# Backend
`tfstate`를 S3와 DynamoDB를 활용하여 관리합니다


###### Provider
```bash
provider "aws" {
  region = "ap-northeast-2" 
  version = "~> 2.49.0"
}
```

###### S3 bucket

```bash
resource "aws_s3_bucket" "tfstate" {
  bucket = "yess-tfstate"

  versioning {
    enabled = true
  }
}
```

> S3에 파일 업로드 & 다운로드
> ```bash
> # 업로드
> aws s3 cp terraform.tfstate s3://terraform/
> 
> # 다운로드
> aws s3 cp s3://terraform/terraform.tfstate .
> ```

###### DynamoDB
```bash
resource "aws_dynamodb_table" "terraform_state_lock" {
  name           = "terraform-lock"
  hash_key       = "LockID"
  billing_mode   = "PAY_PER_REQUEST"

  attribute {
    name = "LockID"
    type = "S"
  }
}
```

###### tfstate 없이 S3통해 Terraform 실행
```bash
terraform {
    backend "s3" { 
      bucket         = "yess-tfstate" 
      key            = "terraform/terraform.tfstate" 
      region         = "ap-northeast-2"  
      encrypt        = true
      dynamodb_table = "terraform-lock"
    }
}
```
