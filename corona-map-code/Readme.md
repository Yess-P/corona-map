<a href="https://gitlab.com/Yess-P/corona-map/-/blob/main/corona-map-code/corona19.ipynb"><img src="https://img.shields.io/badge/Python-3766AB?style=flat-square&logo=Python&logoColor=white"/></a>

# Corona-map 
DevOps 환경 구축 후 테스트를 위해 일별, 지역별로 확진자를 구분하여 지도로 표시하는 corona-map을 만들었습니다. <br>
아래는 Docker 이미지에서 실행되는 python 코드입니다.

<br>

---
![corona-map-real-last](https://user-images.githubusercontent.com/79623220/125631103-78bacab1-ef1b-467c-94eb-5300854e6190.png)
---
