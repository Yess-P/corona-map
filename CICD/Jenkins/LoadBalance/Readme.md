# Jenkins LoadBalance

Private subnet에 위치한 instance에 Jenkins 서버를 구축했다면 접속을 위해 LoadBalancer를 적용해줘야 합니다 <br>
LoadBalance 구성은 `대상 그룹` & `LoadBalance`로 이루어져 있습니다

<br>

### **대상 그룹(Target Group)**


먼저 LoadBalancer가 접속할 대상 그룹(Target Group)을 설정해야 합니다. AWS 카테고리에서 EC2를 클릭 후  로드밸런싱 탭에 있는 대상 그룹으로 접속하고 `대상 그룹 생성`을 누릅니다

<br>


> **그룹 세부 정보 지정(First Page)**

---

첫 번째 화면에 **그룹 세부 정보 지정**에서는 `기본 구성`  `상태 검사`  `태그`를 지정합니다

<br>

---

![target-group](/uploads/de885e0e6a5e194dfe57b706912927a5/target-group.png)
![기본구성](/uploads/e0a7d7b7a8886dd701775e7d7389916d/기본구성.png)

---

![target-group2](/uploads/9645b7d8baaef50a61b92eb111ff1430/target-group2.png)
![상태검사](/uploads/755b6b6c88b38cf68bfd2962e043b8ab/상태검사.png)

---

![target-group3](/uploads/305718c221f057ace34dec1f1add68e1/target-group3.png)
![태그](/uploads/4db5ac52a8235ba38d5b6217721be2c2/태그.png)

<br>

> **대상 등록(Second Page)**

---

두 번째 화면에 **대상 등록**서는 `사용 가능한 인스턴스`를 선택하고 밑에 위치한 `대상` 에 포함시켜 줍니다

<br>

---

![target-group4](/uploads/78639475d5aa4e5ff283737af83b1ff9/target-group4.png)

---

- `사용 가능한 인스턴스`에서 대상(로드밸런서를 통해서 접속할) 인스턴스를 선택합니다
- `선택한 인스턴스를 위한 포트`에 Jenkins가 받을 라우팅받을 포트(default는 8080)를 입력 후 
`아래에 보류 중인 것으로 포함`을 클릭해서 믿에 위치한 `대상` 으로 이동시킵니다

<br>

### LoadBalancer(로드밸런서)

두 번째로 대상 그룹(Target Group)을 로드밸런싱할 로드밸런서를 생성해야 합니다. AWS 카테고리에서 EC2를 클릭 후 로드밸런싱 탭에 있는 로드밸런서로 접속하고 `Load Balancer 생성`을 누릅니다

<br>

> **Load Balancer 유형 선택**

---

`Load Balancer 생성`을 눌러 접속하면 Load Balancer 유형을 선택할 수 있습니다. <br> 
유형에는 `Application Load Balancer(ALB)`  `Network Load Balancer(NLB)`  `Gateway Load Balancer`  `Classic Load Balancer(ELB)` 가 있는데 저희는 HTTP를 사용해 Jenkins 서버로 접속할 것이기 때문에 `Application Load Balancer(ALB)` 를 선택해 주시면 됩니다

<br>

> **Load Balancer 구성**

---

1 단계에서는 `기본구성`  `리스너`  `가용 영역`  `추가서비스` 를 설정해야 합니다

<br>

---

![LB-last](/uploads/e1124338de3e267d015bfaa32ad9a51a/LB-last.png)

---

`기본구성` 은 로드밸런서의 이름, 체계, IP주소 유형을 설정합니다. 이름을 원하시는 이름을 적으시고 나머지는 자신의 환경에 맞게 적용해주시면 됩니다(이름 외에는 default 값으로 두셔도 됩니다)

`리스너` 는 로드밸런서가 어떤 프로토콜 & 어떤 포트의 요청을 받을지 설정합니다. 설정에는 HTTP와 HTTPS가 있는데, 저는 ACN 설정과 Jenkins 서버에서 Jenkins 설정을 바꾸지 않았기 때문에 HTTP를 선택했습니다(제가 설치한 방법은 HTTPS의 default 값이 disable로 되어있어 /etc/sysconfig/jenkins 파일을 수정해야 합니다)

`가용 영역` 은 어느 위치에서 로드밸런서를 가용할지 선택할 수 있습니다. 저의 Jenkins는 kube-vpc-vpc에 위치하기 때문에 kube-vpc-vpc를 선택하였고 Subnet은 Public Subnet에 할당해 주었습니다 Private Subnet에서는 IGW가 없기 때문에 선택하시면 안됩니다

`추가서비스` 는 딱히 설정하지 않아서 넘어 가겠습니다. 설정이 필요하신 분들만 설정해주세요

<br>

> **보안 설정 구성**

---

HTTPS를 사용하지 않기 때문에 넘어 가겠습니다. HTTPS를 사용하시는 분들은 ACN을 선택하시면 됩니다

<br>

> **보안 그룹 구성**

---

![LB-SG](/uploads/7153887b37acd11c6e1ac051b97b12d5/LB-SG.png)

---

보안 그룹은 각자 환경에 맞춰 설정해주시면 됩니다. 설정이 처음이신 분들은 기본으로 설정 해주시면 됩니다

<br>

> **라우팅 구성**

---

![LB-TG](/uploads/68a9cbea236895bf51241fa35aa12386/LB-TG.png)

---

**라우팅구성**에서는 대상 그룹과 상태검사가 있습니다

`대상 그룹`은 처음에 위에서 생성했기 때문에 기존 대상 그룹을 선택하면 밑에 항목들이 자동으로 채워지게 됩니다

`상태 검사` 또한 자동으로 채워집니다

<br>

> **대상 등록 & 검토**

---

대상 등록은 따로 적용할 사항이 없습니다. `다음: 검토` 클릭하시면 됩니다

검토에서는 설정한 값들을 확인하시고 `생성` 하시면 됩니다

<br>

> **로드밸런서 리스너 확인**

---

로드밸런서를 생성하면 리스너가 잘 설정되어 있는지 확인해야 합니다. 로드밸런서 탭에서 생성한 로드밸런서를 클릭 후 밑에 세부사항 탭에서 리스너를 클릭합니다. 리스너 탭을 클릭하면 밑에 리스너 테이블이 보이는데, 테이블에서 생성된 리스너를 클릭하고 편집을 누릅니다

<br>

---

![LB-last](/uploads/e1124338de3e267d015bfaa32ad9a51a/LB-last.png)

---

위의 그림과 같이 설정 되어있어야 합니다. 만약 설정되어 있지 않다면 `작업 추가`를 선택하고 `전달 대상` 클리 후 위에서 만들었던 대상 그룹을 선택하시면 됩니다

<br>

### 번외) 다른 포트 사용을 위한 설정

만약 8080포트가 중복되어 다른 포트를 사용하게 되었다면 방화벽 설정을 통해 포트를 열어주어야 합니다

먼저 Jenkins port를 설정하는 방법입니다

```bash
# Jenkins port 설정
vi /etc/sysconfig/jenkins

# port 변경
# 8080을 9100 port로 변경
JENKINS_PORT="9100"
```

iptables를 쉽게 설정하기 위해 firewall을 설치해야합니다

```bash
# firewalld 설치 & 실행
yum install firewalld
systemctl start firewalld
systemctl enable firewalld

# Jenkins port 사용을 위한 방화벽 open
firewall-cmd --zone=public --add-port=9100/tcp --permanent
firewall-cmd --reload

# 방화벽 open 여부 확인
firewall-cmd --list-ports

# 전체 Port 확인
netstat -tnlp
```
