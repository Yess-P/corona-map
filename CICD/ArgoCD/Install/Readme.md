# **ArgoCD Install**

**1. Install Argo CD**

 ```bash
 # ArgoCD 네임스페이스 생성
 kubectl create namespace argocd

 # ArgoCD 배포
 kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml
 ```

 <br>

**2. Download Argo CD CLI**
 ```bash
 VERSION=$(curl --silent "https://api.github.com/repos/argoproj/argo-cd/releases/latest" | grep '"tag_name"' | sed -E 's/.*"([^"]+)".*/\1/')

 sudo curl -sSL -o /usr/local/bin/argocd https://github.com/argoproj/argo-cd/releases/download/$VERSION/argocd-linux-amd64

 sudo chmod +x /usr/local/bin/argocd
 ```

 <br>

**3. Access The Argo CD API Server**
 ```bash
 # Loadbalance, Ingress, Port Forwarding 세가지 방식이 있다
 # Argo CD Server를 외부에서 접속할 수 있도록 Service의 type을 LoadBalancer로 변경
 kubectl patch svc argocd-server -n argocd -p '{"spec": {"type": "LoadBalancer"}}'

 ```

 <br>

**4. Login Using The CLI**
 ```bash
 # json 파일을 명령어로 검색할 수 있는 패키지 jq 다운로드
 sudo yum install jq -y
	
 # 1.9 버전 이상의 password
 ARGOCD_SERVER=`kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d && echo`

	# 1.8 버전 이하의 password
	ARGOCD_SERVER=`kubectl get pods -n argocd -l app.kubernetes.io/name=argocd-server -o name | cut -d'/' -f 2` 

 # Load Balance 이름
 ARGOCD_SERVER_HOST=`kubectl get svc argocd-server -o json -n argocd | jq -r '.status.loadBalancer.ingress[0].hostname'`

 # 예시
 echo $ARGOCD_SERVER $ARGOCD_SERVER_HOST
 argocd-server-6744f57d55-8mhc7 a4cfe1bb812ad4b2fb23df0c45845c8e-865510826.ap-southeast-1.elb.amazonaws.com

 # 로그인
 argocd login $ARGOCD_SERVER_HOST:80 --grpc-web

 WARNING: server certificate had error: x509: certificate is valid for localhost, argocd-server, argocd-server.argocd, argocd-server.argocd.svc, argocd-server.argocd.svc.cluster.local, 
 not a123456dd12ab11baba0a123a1234567-1234567890.ap-northeast-2.elb.amazonaws.com. Proceed insecurely (y/n)? y
 Username: admin
 Password: # ARGOCD_SERVER와 동일
 'admin' logged in successfully
 Context 'a123456dd12ab11baba0a123a1234567-1234567890.ap-northeast-2.elb.amazonaws.com:80' updated
 # -------------------------------
 # default 계정 정보는 아래와 같음
 # username : admin
 # password : ARGOCD_SERVER와 동일
 # -------------------------------
 # Argo CD 비밀번호 변경
 $ argocd account update-password
 *** Enter current password: # ARGOCD_SERVER와 동일
 *** Enter new password: # 변경할 비밀번호 입력
 *** Confirm new password: # 변경할 비밀번호 재입력(확인)
 ```
